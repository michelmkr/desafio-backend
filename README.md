#Desafio
    Entende-se que o você já tenha criado o ambiente com php >= 7.3 e PostgreSQL - Versão 9.4
# Bibliotecas utizadas
[Symfomy 5.0.*](https://symfony.com/download)

[nelmio/cors-bundle ^2.0](https://github.com/nelmio/NelmioCorsBundle)

#Rodar o projeto
    Acessar o diretório raiz projeto dentro do diretorio raiz rodar os seguintes comandos:
    php bin/console doctrine:migrations:migrate
    
#Na base de dados rodar o comando 
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
    
#Rodar os Testes
    php bin/phpunit
    
#Iniciar o serviço
    symfony server:start
        
# Endpoint - Tipo Produto
    GET - http://localhost/tipo-produto/
    GET - http://localhost/tipo-produto/d2333a74-5b27-11ea-ab33-be0d06f8c7fe
    POST - http://localhost/tipo-produto/
        
        {
        	"descricao" : "Teste"
        }
    PUT - http://localhost/tipo-produto/1a37b602-5b04-11ea-ab33-be0d06f8c7f
    
        {
            "descricao" : "Descricao atualizada"
        }
    DELETE - http://localhost/tipo-produto/0a3bea9c-5b0f-11ea-ab33-be0d06f8c7fe

# Endpoint - Produto
    GET - http://localhost/produto/
    GET - http://localhost/produto/1b6b87da-5b2a-11ea-ab33-be0d06f8c7fe
    POST - http://localhost/produto/
    
        {
        	"codigo" : "12311",
        	"descricao":"Produto 01",
        	"tipo": "10718980-5b64-11ea-97e2-3f3b695d99e7",
        	"quantidade": 0
        }
    PUT - http://localhost/produto/1b6b87da-5b2a-11ea-ab33-be0d06f8c7fe
        
        {
            "codigo" : "1231",
            "descricao":"Produto 01",
            "tipo": "d2333a74-5b27-11ea-ab33-be0d06f8c7fe",
            "quantidade": 0
        } 
     DELETE - http://localhost/produto/1b6b87da-5b2a-11ea-ab33-be0d06f8c7fe

# Endpoint - Venda
    POST - http://localhost/venda/
        
        {
        	"produto" : "ce088588-5c1e-11ea-851e-8c5f35ed0656",
        	"quantidade" : 1
        }
    POST - http://localhost/venda/8a1c5250-5dbd-11ea-aa9e-5ab88e9f7159
    
        {
        	"produto" : "ce088588-5c1e-11ea-851e-8c5f35ed0656",
        	"quantidade" : 1
        }
     
    PUT -http://localhost/venda/b9e702b2-5dab-11ea-aa9e-5ab88e9f7159 
    
        {
        	"quantidade":"22"
        }
        
    DELETE - http://localhost/venda/b9e702b2-5dab-11ea-aa9e-5ab88e9f7159