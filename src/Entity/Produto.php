<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProdutoRepository")
 * @UniqueEntity(fields={"codigo"})
 */
class Produto implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150, unique=true)
     * @Assert\Length(
     *      min = 3,
     *      max = 150
     * )
     */
    private $codigo;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(
     *      min = 3,
     *      max = 100
     * )
     */
    private $descricao;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoProduto", inversedBy="produtos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tipo;

    /**
     * @ORM\Column(type="integer")
     * @Assert\PositiveOrZero()
     */
    private $estoque;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PedidoItem", mappedBy="produto", orphanRemoval=true)
     */
    private $pedidoItems;

    public function __construct()
    {
        $this->pedidoItems = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = trim($codigo);

        return $this;
    }

    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    public function setDescricao(string $descricao): self
    {
        $this->descricao = trim($descricao);

        return $this;
    }

    public function getTipo(): ?TipoProduto
    {
        return $this->tipo;
    }

    public function setTipo(?TipoProduto $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getEstoque(): ?int
    {
        return $this->estoque;
    }

    public function setEstoque(int $estoque): self
    {
        $this->estoque = $estoque;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize() : array
    {
        return [
            'id' => $this->getId(),
            'codigo' => $this->getCodigo(),
            'descricao' => $this->getDescricao(),
            'tipo' => $this->getTipo(),
            'estoque' => $this->getEstoque()
        ];
    }

    /**
     * @return Collection|PedidoItem[]
     */
    public function getPedidoItems(): Collection
    {
        return $this->pedidoItems;
    }

    public function addPedidoItem(PedidoItem $pedidoItem): self
    {
        if (!$this->pedidoItems->contains($pedidoItem)) {
            $this->pedidoItems[] = $pedidoItem;
            $pedidoItem->setProduto($this);
        }

        return $this;
    }

    public function removePedidoItem(PedidoItem $pedidoItem): self
    {
        if ($this->pedidoItems->contains($pedidoItem)) {
            $this->pedidoItems->removeElement($pedidoItem);
            // set the owning side to null (unless already changed)
            if ($pedidoItem->getProduto() === $this) {
                $pedidoItem->setProduto(null);
            }
        }

        return $this;
    }


}
