<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PedidoRepository")
 */
class Pedido implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dataHora;

    /**
     * @ORM\Column(type="guid")
     */
    private $numeroOrdemDeVenda;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PedidoItem", mappedBy="pedido", orphanRemoval=true)
     */
    private $pedidoItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Status", inversedBy="pedidos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    public function __construct()
    {
        $this->pedidoItems = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getDataHora(): ?\DateTimeInterface
    {
        return $this->dataHora;
    }

    public function setDataHora(\DateTimeInterface $dataHora): self
    {
        $this->dataHora = $dataHora;

        return $this;
    }

    public function getNumeroOrdemDeVenda(): ?string
    {
        return $this->numeroOrdemDeVenda;
    }

    public function setNumeroOrdemDeVenda(string $numeroOrdemDeVenda): self
    {
        $this->numeroOrdemDeVenda = $numeroOrdemDeVenda;

        return $this;
    }

    /**
     * @return Collection|PedidoItem[]
     */
    public function getPedidoItems(): Collection
    {
        return $this->pedidoItems;
    }

    public function addPedidoItem(PedidoItem $produtoItem): self
    {
        if (!$this->pedidoItems->contains($produtoItem)) {
            $this->pedidoItems[] = $produtoItem;
            $produtoItem->setPedido($this);
        }

        return $this;
    }

    public function removePedidoItem(PedidoItem $produtoItem): self
    {
        if ($this->pedidoItems->contains($produtoItem)) {
            $this->pedidoItems->removeElement($produtoItem);
            // set the owning side to null (unless already changed)
            if ($produtoItem->getPedido() === $this) {
                $produtoItem->setPedido(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize() : array
    {
        return [
            'id' => $this->getId(),
            'itens' => $this->getPedidoItems()
        ];
    }
}
