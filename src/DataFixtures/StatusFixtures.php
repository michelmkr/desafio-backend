<?php

namespace App\DataFixtures;

use App\Entity\Status;
use App\Entity\TipoProduto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class StatusFixtures extends Fixture
{
    public function load($manager)
    {
        $status = new Status();
        $status->setDescricao("Em Andamento");
        $manager->persist($status);

        $status = new Status();
        $status->setDescricao("Finalizado");
        $manager->persist($status);

        $manager->flush();
    }
}
