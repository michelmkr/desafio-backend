<?php

namespace App\DataFixtures;

use App\Entity\Produto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProdutoFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {

        $cama = $this->getReference(TipoProdutoFixtures::TIPO_PRODUTO_CAMA);
        for($i = 1; $i <= 5; $i++) {
            $produto = new Produto();
            $produto->setDescricao("Produto Cama {$i}");
            $produto->setCodigo(uniqid());
            $produto->setTipo($cama);
            $produto->setEstoque($i * 3);
            $manager->persist($produto);
        }

        $mesa = $this->getReference(TipoProdutoFixtures::TIPO_PRODUTO_MESA);
        for($i = 1; $i <= 5; $i++) {
            $produto = new Produto();
            $produto->setDescricao("Produto Mesa {$i}");
            $produto->setCodigo(uniqid());
            $produto->setTipo($mesa);
            $produto->setEstoque($i * 3);
            $manager->persist($produto);
        }

        $banho = $this->getReference(TipoProdutoFixtures::TIPO_PRODUTO_BANHO);
        for($i = 1; $i <= 5; $i++) {
            $produto = new Produto();
            $produto->setDescricao("Produto Banho {$i}");
            $produto->setCodigo(uniqid());
            $produto->setTipo($banho);
            $produto->setEstoque($i * 3);
            $manager->persist($produto);
        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return array(
            TipoProdutoFixtures::class,
        );
    }
}
