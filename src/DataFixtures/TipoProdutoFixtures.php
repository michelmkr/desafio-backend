<?php

namespace App\DataFixtures;

use App\Entity\Status;
use App\Entity\TipoProduto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TipoProdutoFixtures extends Fixture
{

    const TIPO_PRODUTO_CAMA = 'Cama';
    const TIPO_PRODUTO_MESA = 'Mesa';
    const TIPO_PRODUTO_BANHO = 'Banho';
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $tipoProduto = new TipoProduto();
        $tipoProduto->setDescricao(self::TIPO_PRODUTO_CAMA);
        $manager->persist($tipoProduto);

        $this->addReference(self::TIPO_PRODUTO_CAMA, $tipoProduto);

        $tipoProduto = new TipoProduto();
        $tipoProduto->setDescricao(self::TIPO_PRODUTO_MESA);
        $manager->persist($tipoProduto);

        $this->addReference(self::TIPO_PRODUTO_MESA, $tipoProduto);

        $tipoProduto = new TipoProduto();
        $tipoProduto->setDescricao("Banho");
        $manager->persist($tipoProduto);

        $this->addReference(self::TIPO_PRODUTO_BANHO, $tipoProduto);

        $manager->flush();
    }
}