<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200309013901 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE produto (id UUID NOT NULL, tipo_id UUID NOT NULL, codigo VARCHAR(150) NOT NULL, descricao VARCHAR(100) NOT NULL, estoque INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5CAC49D720332D99 ON produto (codigo)');
        $this->addSql('CREATE INDEX IDX_5CAC49D7A9276E6C ON produto (tipo_id)');
        $this->addSql('CREATE TABLE tipo_produto (id UUID NOT NULL, descricao VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE pedido_item (id UUID NOT NULL, pedido_id UUID NOT NULL, produto_id UUID NOT NULL, quantidade INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6E0730724854653A ON pedido_item (pedido_id)');
        $this->addSql('CREATE INDEX IDX_6E073072105CFD56 ON pedido_item (produto_id)');
        $this->addSql('CREATE TABLE pedido (id UUID NOT NULL, status_id UUID NOT NULL, data_hora TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, numero_ordem_de_venda UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C4EC16CE6BF700BD ON pedido (status_id)');
        $this->addSql('CREATE TABLE status (id UUID NOT NULL, descricao VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE produto ADD CONSTRAINT FK_5CAC49D7A9276E6C FOREIGN KEY (tipo_id) REFERENCES tipo_produto (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pedido_item ADD CONSTRAINT FK_6E0730724854653A FOREIGN KEY (pedido_id) REFERENCES pedido (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pedido_item ADD CONSTRAINT FK_6E073072105CFD56 FOREIGN KEY (produto_id) REFERENCES produto (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pedido ADD CONSTRAINT FK_C4EC16CE6BF700BD FOREIGN KEY (status_id) REFERENCES status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE pedido_item DROP CONSTRAINT FK_6E073072105CFD56');
        $this->addSql('ALTER TABLE produto DROP CONSTRAINT FK_5CAC49D7A9276E6C');
        $this->addSql('ALTER TABLE pedido_item DROP CONSTRAINT FK_6E0730724854653A');
        $this->addSql('ALTER TABLE pedido DROP CONSTRAINT FK_C4EC16CE6BF700BD');
        $this->addSql('DROP TABLE produto');
        $this->addSql('DROP TABLE tipo_produto');
        $this->addSql('DROP TABLE pedido_item');
        $this->addSql('DROP TABLE pedido');
        $this->addSql('DROP TABLE status');
    }
}
