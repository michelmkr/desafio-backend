<?php

namespace App\Exception\Interfaces;

interface CoreException
{
    public function getJsonError();
}