<?php

namespace App\Repository;

use App\Entity\PedidoItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PedidoItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method PedidoItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method PedidoItem[]    findAll()
 * @method PedidoItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProdutoItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PedidoItem::class);
    }

    // /**
    //  * @return ProdutoItem[] Returns an array of ProdutoItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProdutoItem
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
