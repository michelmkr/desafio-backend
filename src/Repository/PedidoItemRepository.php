<?php

namespace App\Repository;

use App\Entity\PedidoItem;
use App\Repository\Common\CoreRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method PedidoItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method PedidoItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method PedidoItem[]    findAll()
 * @method PedidoItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PedidoItemRepository extends CoreRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, $entityManager, PedidoItem::class);
    }

    /**
     * @param string $pedidoId
     * @param string $produtoId
     * @return PedidoItem|null
     */
    public function buscarItemPeloPedidoEProduto(string $pedidoId, string $produtoId)
    {
        return $this->findOneBy([
           'pedido' => $pedidoId,
           'produto' => $produtoId
        ]);
    }
    // /**
    //  * @return PedidoItem[] Returns an array of PedidoItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PedidoItem
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
