<?php

namespace App\Repository\Common\Exception;

use App\Exception\Interfaces\CoreException;
use Throwable;

class RegistroNaoEncontradoException extends \Exception implements CoreException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }


    public function getJsonError()
    {
        return ['errors' => ['RegistroNaoEncontrado' => [$this->getMessage()]]];
    }
}