<?php

namespace App\Repository;

use App\Repository\Common\CoreRepository;
use App\Entity\TipoProduto;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method TipoProduto|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoProduto|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoProduto[]    findAll()
 * @method TipoProduto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoProdutoRepository extends CoreRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, $entityManager, TipoProduto::class);
    }

    // /**
    //  * @return TipoProduto[] Returns an array of TipoProduto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoProduto
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
