<?php

namespace App\Services;

use App\Entity\Pedido;
use App\Entity\Status;
use App\Repository\PedidoRepository;
use App\Services\Common\CoreService;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PedidoService extends CoreService
{
    private $statusService;
    public function __construct(PedidoRepository $pedidoRepository, ValidatorInterface $validator, StatusService $statusService)
    {
        parent::__construct($pedidoRepository, $validator);
        $this->statusService = $statusService;
    }

    /**
     * @return object
     * @throws \Exception
     */
    public function criarPedido()
    {
        /**
         * @var Status
         */
        $status = $this->statusService->findByDescricao("Em Andamento");

        $entity = $this->findByStatus($status->getId());

        if($entity instanceof Pedido)
        {
            return $entity;
        }

        $entity = new Pedido();
        $entity->setStatus($status);
        $entity->setNumeroOrdemDeVenda($this->getGuid());
        $entity->setDataHora(new \DateTime('now'));
        return $this->add($entity);

    }

    /**
     * @param string $id
     * @return object|null
     * @throws \Exception
     */
    public function finalizarPedido(string $id)
    {
        /**
         * @var Status
         */
        $status = $this->statusService->findByDescricao("Finalizado");
        $entity = $this->find($id);
        if($entity == null)
        {
            throw new \Exception("tratar");
        }
        $entity->setStatus($status);
        return $this->update($entity);
    }

    public function findByStatus(string $statusId)
    {
        return $this->entityRepository->findOneBy(
            ['status' => $statusId]
        );
    }
}