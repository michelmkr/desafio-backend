<?php

namespace App\Services;

use App\Repository\Common\CoreRepository;
use App\Repository\StatusRepository;
use App\Services\Common\CoreService;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class StatusService extends CoreService
{

    public function __construct(StatusRepository $entityRepository, ValidatorInterface $validator)
    {
        parent::__construct($entityRepository, $validator);
    }

    /**
     * @param string $descricao
     * @return array
     */
    public function findByDescricao(string $descricao)
    {
        return $this->entityRepository->findOneBy(['descricao' => $descricao]);
    }

}