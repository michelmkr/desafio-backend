<?php
namespace App\Service;

use App\Entity\TipoProduto;
use App\Repository\Common\Exception\RegistroNaoEncontradoException;
use App\Repository\TipoProdutoRepository;
use App\Services\Common\CoreService;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TipoProdutoService extends CoreService
{

    public function __construct(TipoProdutoRepository $produtoRepository, ValidatorInterface $validator)
    {
        parent::__construct($produtoRepository, $validator);
    }

    /**
     * @param string $descricao
     * @return TipoProduto
     */
    public function novoTipoProduto(string $descricao) : TipoProduto
    {
        $tipoProduto = new TipoProduto();
        $tipoProduto->setDescricao($descricao);

        return $this->add($tipoProduto);
    }

    /**
     * @param string $descricao
     * @return TipoProduto
     */
    public function atualizarTipoProduto(string $id, string $descricao) : TipoProduto
    {
        $tipoProduto = $this->find($id);
        $tipoProduto->setDescricao($descricao);

        return $this->update($tipoProduto);
    }
}
