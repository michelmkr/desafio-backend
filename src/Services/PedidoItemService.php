<?php

namespace App\Services;

use App\Entity\PedidoItem;
use App\Repository\PedidoItemRepository;
use App\Repository\PedidoRepository;
use App\Services\Common\CoreService;
use App\Services\Exceptions\ItemJaRegistradoException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PedidoItemService extends CoreService
{

    /**
     * @var PedidoService
     */
    private $pedidoService;

    /**
     * @var ProdutoService
     */
    private $produtoService;

    public function __construct(PedidoItemRepository $pedidoItemRepository,
                                ValidatorInterface $validator,
                                PedidoService $pedidoService,
                                ProdutoService $produtoService)
    {
        parent::__construct($pedidoItemRepository, $validator);
        $this->pedidoService = $pedidoService;
        $this->produtoService = $produtoService;
    }

    /**
     * @param string $pedidoId
     * @param string $produtoId
     * @param int $quantidade
     * @return object
     */
    public function adicionarItem(string $produtoId, int $quantidade)
    {
        $pedido = $this->pedidoService->criarPedido();
        $produto = $this->produtoService->find($produtoId);

        $item = $this->entityRepository->buscarItemPeloPedidoEProduto($pedido->getId(), $produtoId);
        if ($item instanceof PedidoItem) {
            throw new ItemJaRegistradoException("Este item já foi adicionado no carrinho.");
        }

        $pedidoItem = new PedidoItem();
        $pedidoItem->setPedido($pedido)
        ->setProduto($produto)
        ->setQuantidade($quantidade);

        return $this->add($pedidoItem);
    }

    public function atualizarQuantidadeItem(string $id, int $quantidade)
    {
        $entity = $this->find($id);
        if($entity == null)
        {
            throw new \Exception("tratar");
        }

        $entity->setQuantidade($quantidade);
        return $this->update($entity);
    }
    /**
     * @return PedidoService
     */
    public function getPedidoService() : PedidoService
    {
        return $this->pedidoService;
    }

}
