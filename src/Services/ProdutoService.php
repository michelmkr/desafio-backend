<?php

namespace App\Services;

use App\Entity\Produto;
use App\Repository\ProdutoRepository;
use App\Service\TipoProdutoService;
use App\Services\Common\CoreService;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProdutoService extends CoreService
{
    /**
     * @var TipoProdutoService
     */
    private $tipoProdutoService;

    public function __construct(ProdutoRepository $produtoRepository, ValidatorInterface $validator, TipoProdutoService $tipoProdutoService)
    {
        parent::__construct($produtoRepository, $validator);
        $this->tipoProdutoService = $tipoProdutoService;
    }

    public function novoProduto(string $codigoDeBarras, string $descricao, string $tipo, int $estoque)
    {
        $tipoProduto = $this->tipoProdutoService->find($tipo);

        $produto = new Produto();
        $produto->setCodigo($codigoDeBarras)
            ->setDescricao($descricao)
            ->setEstoque($estoque)
            ->setTipo($tipoProduto);

        return $this->add($produto);
    }

    public function atualizarProduto(string $id, string $codigoDeBarras, string $descricao, string $tipo, int $estoque)
    {
        $tipoProduto = $this->tipoProdutoService->find($tipo);
        $produto = $this->find($id);
        $produto->setCodigo($codigoDeBarras)
            ->setDescricao($descricao)
            ->setEstoque($estoque)
            ->setTipo($tipoProduto);

        return $this->update($produto);
    }
}