<?php

namespace App\Services\Exceptions;

use App\Exception\Interfaces\CoreException;

class ItemJaRegistradoException extends  \Exception implements CoreException
{

    public function getJsonError()
    {
        return ['errors' => ['pedidoItem' => $this->getMessage()]];
    }
}