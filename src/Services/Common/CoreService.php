<?php

namespace App\Services\Common;

use App\Repository\Common\CoreRepository;
use App\Services\Common\Exception\ValidacaoServiceException;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Runner\Exception;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class CoreService
{
    protected $entityRepository;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CoreService constructor.
     * @param CoreRepository $entityRepository
     */
    public function __construct(CoreRepository $entityRepository, ValidatorInterface $validator)
    {
        $this->entityRepository = $entityRepository;
        $this->validator = $validator;
    }

    /**
     * @return array
     */
    public function findAll() : array
    {
        return $this->entityRepository->findAll();
    }

    /**
     * @param string $id
     * @return object
     */
    public function find(string $id) : object
    {
        return $this->entityRepository->find($id);
    }

    /**
     * @param object $entity
     * @return object
     */
    public function add(object $entity) : object
    {
        $this->isValid($entity);
        return $this->entityRepository->add($entity);
    }

    /**
     * @param object $entity
     * @return object
     */
    public function update(object $entity) : object
    {
        $this->isValid($entity);
        return $this->entityRepository->update($entity);
    }

    /**
     * @param string $id
     * @return bool
     * @throws \App\Repository\Common\Exception\RegistroNaoEncontradoException
     */
    public function remove(string $id) : bool
    {
        $this->entityRepository->remove($id);
        return true;
    }

    /**
     * @param object $entity
     * @throws ValidacaoServiceException
     */
    protected function isValid(object $entity): void
    {
        $errors = $this->validator->validate($entity);
        if (count($errors) > 0) {
            throw new ValidacaoServiceException($errors);
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function getGuid()
    {
        return vsprintf('%s%s-%s-4000-8%.3s-%s%s%s0',str_split(dechex( microtime(true) * 1000 ) . bin2hex( random_bytes(8) ),4));
    }

}
