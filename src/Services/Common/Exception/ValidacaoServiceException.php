<?php

namespace App\Services\Common\Exception;

use App\Exception\Interfaces\CoreException;
use Throwable;

class ValidacaoServiceException extends \Exception implements CoreException
{
    private $errors;
    public function __construct(object $errors, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->errors = $errors;
        parent::__construct($message, $code, $previous);
    }

    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getJsonError() : array
    {
        $errors = $this->getErrors();
        $resultado['errors'] = [];
        foreach ($errors as $erro)
        {
            $resultado['errors'][$erro->getPropertyPath()][] = $erro->getMessage();
        }

        return $resultado;
    }

}
