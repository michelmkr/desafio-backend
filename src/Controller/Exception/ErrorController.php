<?php

namespace App\Controller\Exception;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

class ErrorController
{


    public function show(\Throwable $throwable, Request $request)
    {
        die($throwable->getMessage());
    }

}