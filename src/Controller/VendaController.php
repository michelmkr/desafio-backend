<?php

namespace App\Controller;

use App\Controller\Common\CoreController;
use App\Services\Common\Exception\ValidacaoServiceException;
use App\Services\Exceptions\ItemJaRegistradoException;
use App\Services\PedidoItemService;
use App\Services\PedidoService;
use Cassandra\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/venda")
 */
class VendaController extends CoreController
{

    public function __construct(PedidoItemService $service)
    {
        parent::__construct($service);
    }

    /**
     * @Route("/", name="venda-get", methods={"GET"})
     */
    public function getAll() : JsonResponse
    {
        return parent::getAll();
    }

    /**
     * @Route("/{id}", name="venda-get-id", methods={"GET"})
     */
    public function getById(string $id): JsonResponse
    {
        return parent::getById($id);
    }

    /**
     * @Route("/", name="venda-post", methods={"POST"})
     */
    public function post(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $quantidade = $data['quantidade'];
        $produto = $data['produto'];

        try
        {
            $status = Response::HTTP_OK;
            $conteudo = $this->getService()->adicionarItem("{$produto}", $quantidade);
        }
        catch(ItemJaRegistradoException | ValidationException $jaRegistradoException)
        {
            $status = Response::HTTP_BAD_REQUEST;
            $conteudo = $jaRegistradoException->getJsonError();
        }

        $response =  new JsonResponse($conteudo, $status);
        $response->headers->add(['content-type'=>'application/json']);
        return $response;
    }

    /**
     * @Route("/{id}", name="venda-put-id", methods={"PUT"})
     */
    public function put(string $id, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $quantidade = $data['quantidade'];
        try
        {
            $status = Response::HTTP_OK;
            $conteudo = $this->getService()->atualizarQuantidadeItem($id, $quantidade);
        }
        catch(ValidacaoServiceException $exception)
        {
            $status = Response::HTTP_BAD_REQUEST;
            $conteudo = $exception->getJsonError();
        }
        $response =  new JsonResponse($conteudo, $status);
        $response->headers->add(['content-type'=>'application/json']);
        return $response;
    }

    /**
     * @Route("/encerrar-pedido/{id}", name="venda2-put-id", methods={"PUT"})
     */
    public function putFinalizarPedido(string $id, Request $request): JsonResponse
    {
        try
        {
            $status = Response::HTTP_OK;
            $conteudo = $this->getService()->atualizarQuantidadeItem($id);
        }
        catch(ValidacaoServiceException $exception)
        {
            $status = Response::HTTP_BAD_REQUEST;
            $conteudo = $exception->getJsonError();
        }
        $response =  new JsonResponse($conteudo, $status);
        $response->headers->add(['content-type'=>'application/json']);
        return $response;
    }

    /**
     * @Route("/{id}", name="venda-delete-id", methods={"DELETE"})
     */
    public function delete(string $id): JsonResponse
    {
        return parent::delete($id);
    }
}
