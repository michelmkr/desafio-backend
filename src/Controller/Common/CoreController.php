<?php

namespace App\Controller\Common;

use App\Repository\Common\Exception\RegistroNaoEncontradoException;
use App\Services\Common\CoreService;
use App\Services\Common\Exception\ValidacaoServiceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class CoreController extends AbstractController
{
    /**
     * @var CoreService
     */
    private $service;

    public function __construct(CoreService $service)
    {
        $this->service = $service;
    }

    /**
     * @return CoreService
     */
    public function getService() : CoreService
    {
        return $this->service;
    }

    /**
     * @return JsonResponse
     */
    public function getAll() : JsonResponse
    {
        $dados = $this->getService()->findAll();
        $response =  new JsonResponse($dados, Response::HTTP_OK);
        $response->headers->add(['content-type'=>'application/json']);
        return $response;
    }

    /**
     * @param string $id
     * @return JsonResponse
     */
    public function getById(string $id) : JsonResponse
    {
        try
        {
            $status = Response::HTTP_OK;
            $dados = $this->getService()->find($id);
        }
        catch(RegistroNaoEncontradoException $registroNaoEncontradoException)
        {
            $status = Response::HTTP_BAD_REQUEST;
            $dados = new \stdClass();
            $dados->errors = [
                'RegistroNaoEncontrado' => $registroNaoEncontradoException->getMessage()
            ];
        }

        $response =  new JsonResponse($dados, $status);
        $response->headers->add(['content-type'=>'application/json']);
        return $response;
    }

    /**
     * @param string $id
     */
    public function delete(string $id) : JsonResponse
    {
        try
        {
            $status = Response::HTTP_OK;
            $this->getService()->remove($id);
            $dados['messages'][] = "Registro removido com sucesso";
        }
        catch(RegistroNaoEncontradoException $registroNaoEncontradoException)
        {
            $status = Response::HTTP_BAD_REQUEST;
            $dados = new \stdClass();
            $dados->errors = [
                'RegistroNaoEncontrado' => $registroNaoEncontradoException->getMessage()
            ];
        }

        $response =  new JsonResponse($dados, $status);
        $response->headers->add(['content-type'=>'application/json']);
        return $response;
    }
    abstract public function post(Request $request) : JsonResponse;
    abstract public function put(string $id, Request $request) : JsonResponse;
}