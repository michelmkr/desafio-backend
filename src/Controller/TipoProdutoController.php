<?php


namespace App\Controller;


use App\Controller\Common\CoreController;
use App\Repository\Common\Exception\RegistroNaoEncontradoException;
use App\Service\TipoProdutoService;
use App\Services\Common\Exception\ValidacaoServiceException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tipo-produto")
 */
class TipoProdutoController extends CoreController
{
    public function __construct(TipoProdutoService $produtoService)
    {
        parent::__construct($produtoService);
    }

    /**
     * @Route("/", name="tipo-produto-get", methods={"GET"})
     */
    public function getAll() : JsonResponse
    {
        return parent::getAll();
    }

    /**
     * @Route("/{id}", name="tipo-produto-get-id", methods={"GET"})
     */
    public function getById(string $id): JsonResponse
    {
        return parent::getById($id);
    }

    /**
     * @Route("/", name="tipo-produto-post", methods={"POST"})
     */
    public function post(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $resultado = [];
        try
        {
            $status = Response::HTTP_CREATED;
            $resultado = $this->getService()->novoTipoProduto("{$data['descricao']}");
        }
        catch(ValidacaoServiceException $validacaoServiceException)
        {
            $status = Response::HTTP_BAD_REQUEST;
            $resultado = $validacaoServiceException->getJsonError();
        }
        $response =  new JsonResponse($resultado, $status);
        $response->headers->add(['content-type'=>'application/json']);
        return $response;
    }

    /**
     * @Route("/{id}", name="tipo-produto-put-id", methods={"PUT"})
     */
    public function put(string $id, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $resultado = [];
        try {
            $status = Response::HTTP_OK;
            $resultado = $this->getService()->atualizarTipoProduto($id, "{$data['descricao']}");
        }
        catch(RegistroNaoEncontradoException | ValidacaoServiceException $exception)
        {
            $status = Response::HTTP_BAD_REQUEST;
            $resultado = $exception->getJsonError();
        }
        $response =  new JsonResponse($resultado, $status);
        $response->headers->add(['content-type'=>'application/json']);
        return $response;
    }

    /**
     * @Route("/{id}", name="tipo-produto-delete-id", methods={"DELETE"})
     */
    public function delete(string $id): JsonResponse
    {
        return parent::delete($id);
    }
}