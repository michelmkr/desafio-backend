<?php

namespace App\Controller;

use App\Controller\Common\CoreController;
use App\Repository\Common\Exception\RegistroNaoEncontradoException;
use App\Services\Common\Exception\ValidacaoServiceException;
use App\Services\ProdutoService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/produto")
 */

class ProdutoController extends CoreController
{
    /**
     * ProdutoController constructor.
     * @param ProdutoService $service
     */
    public function __construct(ProdutoService $service)
    {
        parent::__construct($service);
    }

    /**
     * @Route("/", name="produto-get", methods={"GET"})
     */
    public function getAll() : JsonResponse
    {
        return parent::getAll();
    }

    /**
     * @Route("/{id}", name="produto-get-id", methods={"GET"})
     */
    public function getById(string $id): JsonResponse
    {
        return parent::getById($id);
    }


    /**
     * @Route("/", name="produto-post", methods={"POST"})
     */
    public function post(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $resultado = [];
        try
        {
            $codigo = $data['codigo'];
            $descricao = $data['descricao'];
            $tipo = $data['tipo'];
            $quantidade = intval($data['quantidade']);
            $status = Response::HTTP_CREATED;

            $resultado = $this->getService()->novoProduto("{$codigo}", "{$descricao}", "{$tipo}", $quantidade);
        }
        catch(RegistroNaoEncontradoException | ValidacaoServiceException $exception)
        {
            $status = Response::HTTP_BAD_REQUEST;
            $resultado = $exception->getJsonError();
        }
        $response =  new JsonResponse($resultado, $status);
        $response->headers->add(['content-type'=>'application/json']);
        return $response;
    }

    /**
     * @Route("/{id}", name="produto-put-id", methods={"PUT"})
     */
    public function put(string $id, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $codigo = $data['codigo'];
        $descricao = $data['descricao'];
        $tipo = $data['tipo'];
        $quantidade = intval($data['quantidade']);
        $resultado = [];
        try {
            $status = Response::HTTP_OK;
            $resultado = $this->getService()->atualizarProduto($id, "{$codigo}", "{$descricao}", "{$tipo}", $quantidade);
        }
        catch (RegistroNaoEncontradoException $registroNaoEncontradoException)
        {
            $status = Response::HTTP_BAD_REQUEST;
            $resultado['errors']['RegistroNaoEncontrado'] = $registroNaoEncontradoException->getMessage();
        }
        catch(ValidacaoServiceException $validacaoServiceException)
        {
            $status = Response::HTTP_BAD_REQUEST;
            $errors = $validacaoServiceException->getErrors();
            $resultado['errors'] = [];
            foreach ($errors as $erro)
            {
                $resultado['errors'][$erro->getPropertyPath()][] = $erro->getMessage();
            }
        }
        $response =  new JsonResponse($resultado, $status);
        $response->headers->add(['content-type'=>'application/json']);
        return $response;
    }

    /**
     * @Route("/{id}", name="produto-delete-id", methods={"DELETE"})
     */
    public function delete(string $id): JsonResponse
    {
        return parent::delete($id);
    }
}
