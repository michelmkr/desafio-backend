<?php

namespace App\Tests\Services;

use App\Entity\Produto;
use App\Entity\TipoProduto;
use App\Repository\ProdutoRepository;
use App\Repository\TipoProdutoRepository;
use App\Service\TipoProdutoService;
use App\Services\Common\Exception\ValidacaoServiceException;
use App\Services\ProdutoService;

class ProdutoServiceTest extends  BaseTest
{
    public function testAdd()
    {
        $tipoProduto = new TipoProduto();
        $tipoProduto->setId($this->guid());
        $tipoProduto->setDescricao("teste");

        // arrange
        $produto = new Produto();
        $produto->setId($this->guid());
        $produto->setCodigo("000000000000001");
        $produto->setDescricao("Produto 01");
        $produto->setTipo($tipoProduto);
        $produto->setEstoque(1);

        // act
        $reprository = $this->createMock(ProdutoRepository::class);
        $reprository->expects($this->any())
            ->method('add')
            ->willReturn($produto);

        $tipoProdutoService = $this->createMock(TipoProdutoService::class);
        $tipoProdutoService->expects($this->any())
            ->method('find')
            ->willReturn($tipoProduto);


        $service = new ProdutoService($reprository, $this->validator, $tipoProdutoService);
        $novoProduto = $service->novoProduto(
            $produto->getCodigo(),
            $produto->getDescricao(),
            $produto->getTipo()->getId(),
            $produto->getEstoque()
        );

        // assert
        $this->assertEquals($produto->getDescricao(), $novoProduto->getDescricao());
        $this->assertEquals($produto->getCodigo(), $novoProduto->getCodigo());
        $this->assertEquals($produto->getTipo(), $novoProduto->getTipo());
        $this->assertEquals($produto->getId(), $novoProduto->getId());

        return $novoProduto;
    }

    /**
     * @return \Generator
     */
    public function additionProvider()
    {
        yield ["", "", "", -1];
        yield ["1", "", "", 0];
        yield ["1", "1", "", 0];
        yield ["1", "1", "1", 0];
        yield ["123", "", "", 0];
        yield ["123", "", "123", 0];
        yield ["", "", "123", 0];
        yield ["", "123", "123", 0];
        yield ["123", "123", "123", -10];
        yield ["    ", "     ", "123", 1];
        yield ["    ", str_repeat("1",200), "123", 1];
        yield [str_repeat("1",200),"123123", "123", 1];
    }

    /**
     * @dataProvider additionProvider
     */
    public function testValidacaoValoresInvalidos(string $codigo, string $descricao, string $tipo, int $estoque)
    {
        $tipoProduto = new TipoProduto();
        $tipoProduto->setId($tipo);
        $tipoProduto->setDescricao("teste");

        // arrange
        $produto = new Produto();
        $produto->setId($this->guid());
        $produto->setCodigo($codigo);
        $produto->setDescricao($descricao);
        $produto->setTipo($tipoProduto);
        $produto->setEstoque($estoque);

        // act
        $reprository = $this->createMock(ProdutoRepository::class);
        $reprository->expects($this->any())
            ->method('add')
            ->willReturn($produto);

        $tipoProdutoService = $this->createMock(TipoProdutoService::class);
        $tipoProdutoService->expects($this->any())
            ->method('find')
            ->willReturn($tipoProduto);

        $service = new ProdutoService($reprository, $this->validator, $tipoProdutoService);

        try
        {
            $novoProduto = $service->novoProduto(
                $produto->getCodigo(),
                $produto->getDescricao(),
                $produto->getTipo()->getId(),
                $produto->getEstoque()
            );
            /*Para quebrar o teste caso ocorra de gravar corretamente*/
            $this->assertTrue(false);
        }
        catch (ValidacaoServiceException $validacaoServiceException)
        {
            $this->assertTrue(true);
        }

    }
}