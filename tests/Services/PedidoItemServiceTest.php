<?php

namespace App\Tests\Services;

use App\Entity\Pedido;
use App\Entity\PedidoItem;
use App\Entity\Produto;
use App\Entity\Status;
use App\Entity\TipoProduto;
use App\Repository\PedidoItemRepository;
use App\Services\Common\Exception\ValidacaoServiceException;
use App\Services\PedidoItemService;
use App\Services\PedidoService;
use App\Services\ProdutoService;
use App\Services\StatusService;

class PedidoItemServiceTest extends BaseTest
{
    public function testAdd()
    {
        $tipoProduto = new TipoProduto();
        $tipoProduto->setId($this->guid());
        $tipoProduto->setDescricao("teste");

        // arrange
        $produto = new Produto();
        $produto->setId($this->guid());
        $produto->setCodigo("000000000000001");
        $produto->setDescricao("Produto 01");
        $produto->setTipo($tipoProduto);
        $produto->setEstoque(1);

        $pedido = new Pedido();
        $pedido->setid($this->guid());
        $pedido->setNumeroOrdemDeVenda($this->guid());

        $pedidoItem = new PedidoItem();
        $pedidoItem->setPedido($pedido);
        $pedidoItem->setProduto($produto);
        $pedidoItem->setQuantidade(1);

        // act
        $reprository = $this->createMock(PedidoItemRepository::class);
        $reprository->expects($this->any())
            ->method('add')
            ->willReturn($pedidoItem);

        $reprository->expects($this->any())
            ->method('buscarItemPeloPedidoEProduto')
            ->willReturn(new \stdClass());

        $produtoService = $this->createMock(ProdutoService::class);
        $produtoService->expects($this->any())
            ->method('find')
            ->willReturn($produto);

        $pedidoService = $this->createMock(PedidoService::class);
        $pedidoService->expects($this->any())
            ->method('criarPedido')
            ->willReturn($pedido);

        $status = new Status();
        $status->setId($this->guid());

        $statusService = $this->createMock(StatusService::class);
        $statusService->expects($this->any())
            ->method('findByDescricao')
            ->willReturn($status);

        $service = new PedidoItemService($reprository, $this->validator, $pedidoService, $produtoService, $statusService);
        $statusService = $this->createMock(PedidoService::class);
        $statusService->expects($this->any())
            ->method('criarPedido')
            ->willReturn($pedido);


        $pedidoItemNovo = $service->adicionarItem($produto->getId(), 1);

        // assert
        $this->assertEquals($pedidoItem->getId(), $pedidoItemNovo->getId());
        $this->assertEquals($pedidoItem->getPedido()->getId(), $pedido->getId());
        $this->assertEquals($pedidoItem->getProduto()->getId(), $produto->getId());
    }

    /**
     * @return \Generator
     */
    public function additionProvider()
    {
        yield [-1, false];
        yield [0, false];
        yield [1, true];
        yield [2, true];
    }

    /**
     * @dataProvider additionProvider
     */
    public function testValidacaoValoresInvalidos(int $quantidade, bool $assert)
    {
        $tipoProduto = new TipoProduto();
        $tipoProduto->setId($this->guid());
        $tipoProduto->setDescricao("teste");

        // arrange
        $produto = new Produto();
        $produto->setId($this->guid());
        $produto->setCodigo("000000000000001");
        $produto->setDescricao("Produto 01");
        $produto->setTipo($tipoProduto);
        $produto->setEstoque(1);

        $pedido = new Pedido();
        $pedido->setid($this->guid());
        $pedido->setNumeroOrdemDeVenda($this->guid());

        $pedidoItem = new PedidoItem();
        $pedidoItem->setPedido($pedido);
        $pedidoItem->setProduto($produto);
        $pedidoItem->setQuantidade(1);

        // act
        $reprository = $this->createMock(PedidoItemRepository::class);
        $reprository->expects($this->any())
            ->method('add')
            ->willReturn($pedidoItem);

        $reprository->expects($this->any())
            ->method('buscarItemPeloPedidoEProduto')
            ->willReturn(new \stdClass());

        $produtoService = $this->createMock(ProdutoService::class);
        $produtoService->expects($this->any())
            ->method('find')
            ->willReturn($produto);

        $pedidoService = $this->createMock(PedidoService::class);
        $pedidoService->expects($this->any())
            ->method('criarPedido')
            ->willReturn($pedido);

        $status = new Status();
        $status->setId($this->guid());

        $statusService = $this->createMock(StatusService::class);
        $statusService->expects($this->any())
            ->method('findByDescricao')
            ->willReturn($status);

        $service = new PedidoItemService($reprository, $this->validator, $pedidoService, $produtoService, $statusService);

        try
        {
            $pedidoItemNovo = $service->adicionarItem($produto->getId(), $quantidade);
            // assert
            $this->assertTrue($assert);
        }
        catch(ValidacaoServiceException $validacaoServiceException)
        {
            // assert
            $this->assertTrue(true);
        }
    }
}
