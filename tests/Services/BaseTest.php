<?php

namespace App\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class BaseTest extends  WebTestCase
{
    protected $validator;
    protected function setUp()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();
        $this->validator = $container->get('validator');
    }

    protected function guid() : string
    {
        return vsprintf('%s%s-%s-4000-8%.3s-%s%s%s0',str_split(dechex( microtime(true) * 1000 ) . bin2hex( random_bytes(8) ),4));
    }
}