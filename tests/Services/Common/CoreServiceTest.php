<?php

namespace App\Tests\Services\Common;

use App\Entity\TipoProduto;
use App\Repository\TipoProdutoRepository;
use App\Service\TipoProdutoService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CoreServiceTest extends WebTestCase
{

    private $validator;
    protected function setUp()
    {
        self::bootKernel();
        $container = self::$kernel->getContainer();
        $this->validator = $container->get('validator');
    }

    private function guid() : string
    {
        return vsprintf('%s%s-%s-4000-8%.3s-%s%s%s0',str_split(dechex( microtime(true) * 1000 ) . bin2hex( random_bytes(8) ),4));
    }
    /**
     * Testa o Find All do Core
     * Será utilizado como base o repositório de TipoProduto
     */
    public function testFindAll()
    {
        // arrange
        $tipoProduto1 = new TipoProduto();
        $tipoProduto1->setId($this->guid());
        $tipoProduto1->setDescricao("Teste01");

        $tipoProduto2 = new TipoProduto();
        $tipoProduto2->setId($this->guid());
        $tipoProduto2->setDescricao("Teste02");

        ///act
        $reprository = $this->createMock(TipoProdutoRepository::class);
        $reprository->expects($this->any())
            ->method('findAll')
            ->willReturn([$tipoProduto1, $tipoProduto2]);

        $service = new TipoProdutoService($reprository, $this->validator);
        $findAll = $service->findAll();

        // assert
        $this->assertCount(2,$findAll);
        $this->assertEquals($findAll[0]->getId(), $tipoProduto1->getId());
        $this->assertEquals($findAll[1]->getDescricao(), $tipoProduto2->getDescricao());

    }

    public function testFind()
    {
        // arrange
        $tipoProduto1 = new TipoProduto();
        $tipoProduto1->setId($this->guid());
        $tipoProduto1->setDescricao("Teste01");

        ///act
        $reprository = $this->createMock(TipoProdutoRepository::class);
        $reprository->expects($this->any())
            ->method('find')
            ->willReturn($tipoProduto1);

        $service = new TipoProdutoService($reprository, $this->validator);
        $findById = $service->find($tipoProduto1->getId());

        // assert
        $this->assertEquals($tipoProduto1,$findById);
        $this->assertEquals($findById->getDescricao(), $tipoProduto1->getDescricao());
        $this->assertEquals($findById->getId(), $tipoProduto1->getId());

    }

    public function testAdd()
    {
        // arrange
        $tipoProduto1 = new TipoProduto();
        $tipoProduto1->setId($this->guid());
        $tipoProduto1->setDescricao("TesteAdd");

        ///act
        $reprository = $this->createMock(TipoProdutoRepository::class);
        $reprository->expects($this->any())
            ->method('add')
            ->willReturn($tipoProduto1);

        $service = new TipoProdutoService($reprository, $this->validator);
        $object = $service->add($tipoProduto1);

        // assert
        $this->assertEquals($tipoProduto1,$object);
        $this->assertEquals($object->getDescricao(), $tipoProduto1->getDescricao());
        $this->assertEquals($object->getId(), $tipoProduto1->getId());

        return $tipoProduto1->getId();
    }

    public function testUpdate()
    {
        // arrange
        $tipoProduto1 = new TipoProduto();
        $tipoProduto1->setId($this->guid());
        $tipoProduto1->setDescricao("TesteUpdate");

        ///act
        $reprository = $this->createMock(TipoProdutoRepository::class);
        $reprository->expects($this->any())
            ->method('update')
            ->willReturn($tipoProduto1);

        $service = new TipoProdutoService($reprository, $this->validator);
        $object = $service->update($tipoProduto1);

        // assert
        $this->assertEquals($tipoProduto1,$object);
        $this->assertEquals($object->getDescricao(), $tipoProduto1->getDescricao());
        $this->assertEquals($object->getId(), $tipoProduto1->getId());
    }

    /**
     * @depends testAdd
     */
    public function testRemove(string $id)
    {

        ///act
        $reprository = $this->createMock(TipoProdutoRepository::class);
        $reprository->expects($this->any())
            ->method('remove')
            ->willReturn(true);

        $service = new TipoProdutoService($reprository, $this->validator);
        $object = $service->remove($id);

        // assert
        $this->assertTrue($object);
    }
}