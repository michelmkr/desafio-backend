<?php

namespace App\Tests\Services;

use App\Entity\Pedido;
use App\Entity\Status;
use App\Repository\PedidoItemRepository;
use App\Repository\PedidoRepository;
use App\Services\PedidoItemService;
use App\Services\PedidoService;
use App\Services\StatusService;

class PedidoServiceTest extends BaseTest
{
    public function testFindByStatus()
    {
        // arrange
        $status = new Status();
        $status->setId($this->guid());
        $status->setDescricao("Teste");

        // act
        $reprository = $this->createMock(PedidoRepository::class);
        $statusService = $this->createMock(StatusService::class);
        $reprository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($status);

        $service = new PedidoService($reprository,$this->validator,$statusService);
        /**
         * @var Status
         */
        $retorno = $service->findByStatus("");

        // assert
        $this->assertEquals( $status->getDescricao(), $retorno->getDescricao());
        $this->assertEquals( $status->getId(), $retorno->getID());

    }

    /**
     * @return \Generator
     */
    public function additionProvider()
    {
        yield ["add"];
        yield ["findOneBy"];
    }

    /**
     * @dataProvider additionProvider
     */
    public function testCriarPedido(string $metodo)
    {

        // arrange
        $status = new Status();
        $status->setId($this->guid());
        $status->setDescricao("Teste");

        $pedido = new Pedido();
        $pedido->setId($this->guid());
        $pedido->setDataHora(new \DateTime());
        $pedido->setNumeroOrdemDeVenda($this->guid());
        $pedido->setStatus($status);

        // act
        $statusService = $this->createMock(StatusService::class);
        $statusService->expects($this->any())
            ->method('findByDescricao')
            ->willReturn($status);

        $reprository = $this->createMock(PedidoRepository::class);
        $reprository->expects($this->any())
            ->method($metodo)
            ->willReturn($pedido);

        $service = new PedidoService($reprository,$this->validator,$statusService);
        /**
         * @var Status
         */
        $retorno = $service->criarPedido();

        // assert
        $this->assertEquals($pedido->getId(), $retorno->getId());
        $this->assertEquals($pedido->getNumeroOrdemDeVenda(), $retorno->getNumeroOrdemDeVenda());
    }


    public function testFinalizarPedido()
    {
        // arrange
        $status = new Status();
        $status->setId($this->guid());
        $status->setDescricao("Em Andamento");

        $pedido = new Pedido();
        $pedido->setId($this->guid());
        $pedido->setDataHora(new \DateTime());
        $pedido->setNumeroOrdemDeVenda($this->guid());
        $pedido->setStatus($status);


        // act
        $statusService = $this->createMock(StatusService::class);
        $status1 = new Status();
        $status1->setId($this->guid());
        $status1->setDescricao("Finalizado");
        $statusService->expects($this->any())
            ->method('findByDescricao')
            ->willReturn($status1);

        $reprository = $this->createMock(PedidoRepository::class);
        $reprository->expects($this->any())
            ->method("find")
            ->willReturn($pedido);

        $reprository->expects($this->any())
            ->method("update")
            ->willReturn($pedido);

        $service = new PedidoService($reprository,$this->validator,$statusService);

        /**
         * @var Status
         */
        $retorno = $service->finalizarPedido($pedido->getId());
        // assert
        $this->assertEquals($pedido->getId(), $retorno->getId());
        $this->assertEquals($pedido->getNumeroOrdemDeVenda(), $retorno->getNumeroOrdemDeVenda());
        $this->assertEquals($pedido->getStatus()->getDescricao(), $status1->getDescricao());
    }
}
