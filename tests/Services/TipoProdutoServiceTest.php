<?php

namespace App\Tests\Services;

use App\Entity\TipoProduto;
use App\Repository\TipoProdutoRepository;
use App\Service\TipoProdutoService;
use App\Services\Common\Exception\ValidacaoServiceException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TipoProdutoServiceTest extends BaseTest
{
    public function testAdd()
    {
        // arrange
        $tipoProduto = new TipoProduto();
        $tipoProduto->setId($this->guid());
        $tipoProduto->setDescricao("Primeiro Registro");

        // act
        $reprository = $this->createMock(TipoProdutoRepository::class);
        $reprository->expects($this->any())
            ->method('add')
            ->willReturn($tipoProduto);

        $service = new TipoProdutoService($reprository, $this->validator);
        $novoTipoProduto = $service->novoTipoProduto($tipoProduto->getDescricao());

        // assert
        $this->assertEquals($novoTipoProduto->getDescricao(), $tipoProduto->getDescricao());

        return $tipoProduto;
    }

    public function testValidacaoDescricaoVazia()
    {
        // arrange
        $descricao =  "";
        // act
        $reprository = $this->createMock(TipoProdutoRepository::class);

        $service = new TipoProdutoService($reprository, $this->validator);

        try
        {
            $novoTipoProduto = $service->novoTipoProduto($descricao);
        }
        catch (ValidacaoServiceException $validacao)
        {
            // assert
            $errors = $validacao->getErrors();
            $this->assertTrue(true);
            $this->assertCount(1, $errors);
            $this->assertEquals("This value is too short. It should have 3 characters or more.", $errors[0]->getMessage());
        }

    }

    public function testValidacaoDescricaoComMaisDe50Caracteres()
    {
        // arrange
        $descricao =  str_repeat("A",100);

        // act
        $reprository = $this->createMock(TipoProdutoRepository::class);

        $service = new TipoProdutoService($reprository, $this->validator);

        try
        {
            $novoTipoProduto = $service->novoTipoProduto($descricao);
        }
        catch (ValidacaoServiceException $validacao)
        {
            // assert
            $errors = $validacao->getErrors();
            $this->assertTrue(true);
            $this->assertCount(1, $errors);
            $this->assertEquals("This value is too long. It should have 50 characters or less.", $errors[0]->getMessage());
        }
    }

    /**
     * @depends testAdd
     */
    public function testUpdate(TipoProduto $tipoProduto)
    {
        // act
        $reprository = $this->createMock(TipoProdutoRepository::class);
        $reprository->expects($this->any())
            ->method('find')
            ->willReturn($tipoProduto);

        $tipoProdutoModificado = clone $tipoProduto;
        $tipoProdutoModificado->setDescricao("Teste Modificacao");
        $reprository->expects($this->any())
            ->method('update')
            ->willReturn($tipoProdutoModificado);

        $service = new TipoProdutoService($reprository, $this->validator);
        $produtoAtualizado = $service->atualizarTipoProduto($tipoProdutoModificado->getId(), $tipoProdutoModificado->getDescricao());

        // assert
        $this->assertEquals($produtoAtualizado->getDescricao(), $tipoProdutoModificado->getDescricao());
    }

    /**
     * @depends testAdd
     */
    public function testValidacaoAoAtualizarOProduto(TipoProduto $tipoProduto)
    {
        // act
        $reprository = $this->createMock(TipoProdutoRepository::class);
        $reprository->expects($this->any())
            ->method('find')
            ->willReturn($tipoProduto);


        $service = new TipoProdutoService($reprository, $this->validator);

        try
        {
            $produtoAtualizado = $service->atualizarTipoProduto($tipoProduto->getId(), "");
        }
        catch(ValidacaoServiceException $validacaoServiceException)
        {
            // assert
            $errors = $validacaoServiceException->getErrors();
            $this->assertTrue(true);
            $this->assertCount(1, $errors);
            $this->assertEquals("This value is too short. It should have 3 characters or more.", $errors[0]->getMessage());
        }

    }

}
