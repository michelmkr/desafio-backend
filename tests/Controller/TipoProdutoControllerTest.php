<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TipoProdutoControllerTest extends WebTestCase
{

    protected function setUp()
    {
        parent::setUp();
    }

    public function testPost()
    {
        //arrange
        $conteudo = json_encode(['descricao'=>'teste']);

        //act
        $client = static::createClient();
        $client->xmlHttpRequest(
            'POST',
            '/tipo-produto/',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $conteudo
        );

        $entity = json_decode($client->getResponse()->getContent());

        //assert
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $this->assertNotNull($entity->id);
        $this->assertEquals($entity->descricao,"teste");

        return $entity->id;
    }

    /**
     * @depends testPost
     */
    public function testGet(string $id)
    {
        // arrange

        //act
        $client = static::createClient();
        $client->xmlHttpRequest(
            'GET',
            '/tipo-produto/'.$id,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $entity = json_decode($client->getResponse()->getContent());

        //assert
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals("teste", $entity->descricao);
        $this->assertEquals($id, $entity->id);
        return $id;
    }

    /**
     * @depends testGet
     */
    public function testUpdate(string $id)
    {
        // arrange
        $conteudo = json_encode(['descricao'=>'teste atualizado']);

        // act
        $client = static::createClient();
        $client->xmlHttpRequest(
            'PUT',
            '/tipo-produto/'.$id,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $conteudo
        );

        $entity = json_decode($client->getResponse()->getContent());

        // assert
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals("teste atualizado", $entity->descricao);
        $this->assertEquals($id, $entity->id);

        return $id;
    }

    /**
     * @depends testUpdate
     */
    public function testRemove(string $id)
    {
        // arrange
        $client = static::createClient();

        // act
        $client->xmlHttpRequest(
            'DELETE',
            '/tipo-produto/'.$id,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $entity = json_decode($client->getResponse()->getContent());

        // assert
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }


    public function additionProvider()
    {
        yield [""];
        yield [null];
        yield ["                    "];
    }

    /**
     * @dataProvider additionProvider
     */
    public function testValidacaoRegistroVazio($descricao)
    {
        //arrange
        $conteudo = json_encode(['descricao'=>$descricao]);

        //act
        $client = static::createClient();
        $client->xmlHttpRequest(
            'POST',
            '/tipo-produto/',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $conteudo
        );

        $errors = json_decode($client->getResponse()->getContent());

        //assert
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        $this->assertTrue(isset($errors->errors));
        $this->assertTrue(isset($errors->errors->descricao));
        $this->assertEquals(1, count($errors->errors->descricao));
        $this->assertEquals('This value is too short. It should have 3 characters or more.', $errors->errors->descricao[0]);
    }


}